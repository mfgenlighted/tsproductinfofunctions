﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TSProductInfoFunctionsDLL
{
    public partial class SelectProduct : Form
    {
        public string PickedBenchMarkHlaPn = string.Empty;

        public SelectProduct(ProductData[] productData)
        {
            ListViewItem lvitem = new ListViewItem();
            InitializeComponent();
            List<string> productList = new List<string>();

            foreach (var item in productData)
            {
                lvitem = new ListViewItem(item.HlaPartNumber);
                lvitem.SubItems.Add(item.BenchMarkPfsCode);
                lvitem.SubItems.Add(item.ProductDescription);
                listView1.Items.Add(lvitem);
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            PickedBenchMarkHlaPn = listView1.SelectedItems[0].Text;
            DialogResult = DialogResult.OK;
        }
    }
}
